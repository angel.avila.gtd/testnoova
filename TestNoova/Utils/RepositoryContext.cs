﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Models;

namespace TestNoova
{
    public class RepositoryContext: DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Customer>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Address>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Contact>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Phone>().HasQueryFilter(x => !x.Deleted);
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Phone> Phones { get; set; }
    }
}
