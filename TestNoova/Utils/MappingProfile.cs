﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.DataTransferObjects;
using TestNoova.Models;

namespace TestNoova.Utils
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerDto>();
            CreateMap<NewCustomerDto, Customer>();
            CreateMap<UpdateCustomerDto, Customer>();
            CreateMap<Address, AddressDto>();
            CreateMap<NewAddressDto, Address>();
            CreateMap<UpdateAddressDto, Address>();
            CreateMap<Contact, ContactDto>();
            CreateMap<NewContactDto, Contact>();
            CreateMap<UpdateContactDto, Contact>();
            CreateMap<Phone, PhoneDto>();
            CreateMap<NewPhoneDto, Phone>();
            CreateMap<UpdatePhoneDto, Phone>();
        }
    }
}
