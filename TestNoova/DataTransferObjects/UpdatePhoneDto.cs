﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.DataTransferObjects
{
    public class UpdatePhoneDto
    {
        [StringLength(5, ErrorMessage = "El código del país no puede ser mayor a 5 caracteres.")]
        public string CountryCode { get; set; }

        [StringLength(5, ErrorMessage = "El código de la subdivisión no puede ser mayor a 5 caracteres.")]
        public string SubdivisionCode { get; set; }

        [Required(ErrorMessage = "El número de teléfono es requerido.")]
        [StringLength(20, ErrorMessage = "El número de teléfono no puede ser mayor a 20 caracteres.")]
        public string PhoneNumber { get; set; }

        [StringLength(50, ErrorMessage = "La extensión no puede ser mayor a 50 caracteres.")]
        public string Extension { get; set; }

        [Required(ErrorMessage = "El cliente referenciado es requerido.")]
        public Guid IdCustomer { get; set; }
    }
}
