﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.DataTransferObjects
{
    public class AddressDto
    {
        public Guid Id { get; set; }
        public string Country { get; set; }
        public string Subdivision { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string ShortAddress { get; set; }
        public Guid IdCustomer { get; set; }
    }
}
