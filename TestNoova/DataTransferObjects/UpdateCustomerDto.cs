﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.DataTransferObjects
{
    public class UpdateCustomerDto
    {
        [Required(ErrorMessage = "El nombre es requerido.")]
        [StringLength(200, ErrorMessage = "El nombre no puede ser mayor a 200 caracteres.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "El correo es requerido.")]
        [StringLength(150, ErrorMessage = "El correo no puede ser mayor a 150 caracteres.")]
        [EmailAddress(ErrorMessage = "Por favor ingrese un correo valido.")]
        public string Email { get; set; }
    }
}
