﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.DataTransferObjects
{
    public class PhoneDto
    {
        public Guid Id { get; set; }
        public string CountryCode { get; set; }
        public string SubdivisionCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        public Guid IdCustomer { get; set; }
    }
}
