﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.DataTransferObjects
{
    public class UpdateContactDto
    {
        [Required(ErrorMessage = "El nombre es requerido.")]
        [StringLength(200, ErrorMessage = "El nombre no puede ser mayor a 200 caracteres.")]
        public string FullName { get; set; }

        [StringLength(20, ErrorMessage = "El teléfono no puede ser mayor a 20 caracteres.")]
        public string Phone { get; set; }

        [StringLength(50, ErrorMessage = "La extensión no puede ser mayor a 50 caracteres.")]
        public string Extension { get; set; }

        [Required(ErrorMessage = "El teléfono móvil es requerido.")]
        [StringLength(20, ErrorMessage = "El teléfono móvil no puede ser mayor a 20 caracteres.")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "El correo es requerido.")]
        [StringLength(150, ErrorMessage = "El correo no puede ser mayor a 150 caracteres.")]
        [EmailAddress(ErrorMessage = "Por favor ingrese un correo valido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "El cliente referenciado es requerido.")]
        public Guid IdCustomer { get; set; }
    }
}
