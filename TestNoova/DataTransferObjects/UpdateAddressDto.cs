﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.DataTransferObjects
{
    public class UpdateAddressDto
    {
        [Required(ErrorMessage = "El país es requerido.")]
        [StringLength(50, ErrorMessage = "El país no puede ser mayor a 50 caracteres.")]
        public string Country { get; set; }

        [Required(ErrorMessage = "La subdivisión es requerida.")]
        [StringLength(50, ErrorMessage = "La subdivisión no puede ser mayor a 50 caracteres.")]
        public string Subdivision { get; set; }

        [Required(ErrorMessage = "La ciudad es requerida.")]
        [StringLength(50, ErrorMessage = "La ciudad no puede ser mayor a 50 caracteres.")]
        public string City { get; set; }

        [StringLength(20, ErrorMessage = "El código postal no puede ser mayor a 20 caracteres.")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "La dirección es requerida.")]
        [StringLength(300, ErrorMessage = "La dirección no puede ser mayor a 300 caracteres.")]
        public string ShortAddress { get; set; }

        [Required(ErrorMessage = "El cliente referenciado es requerido.")]
        public Guid IdCustomer { get; set; }
    }
}
