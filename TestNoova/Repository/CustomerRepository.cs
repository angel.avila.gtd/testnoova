﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Contracts;
using TestNoova.Models;

namespace TestNoova.Repository
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Customer>> GetAllCustomers()
        {
            return await FindAll()
                .OrderBy(c => c.CreationDate)
                .Include(a => a.Addresses)
                .Include(cn => cn.Contacts)
                .Include(p => p.Phones)
                .ToListAsync();
        }

        public async Task<IEnumerable<Customer>> GetAllCustomersByFilter(string filter)
        {
            return await FindAll()
               .OrderBy(c => c.CreationDate)
               .Include(a => a.Addresses)
               .Include(cn => cn.Contacts)
               .Include(p => p.Phones)
               .Where(c => EF.Functions.Like(c.FullName, $"%{filter}%"))
               .ToListAsync();
        }

        public async Task<Customer> GetCustomerById(Guid customerId)
        {
            return await FindByCondition(c => c.Id.Equals(customerId))
                .Include(a => a.Addresses)
                .Include(cn => cn.Contacts)
                .Include(p => p.Phones)
                .FirstOrDefaultAsync();
        }

        public void CreateCustomer(Customer customer)
        {
            customer.CreationDate = DateTime.Now;
            Create(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            Update(customer);
        }

        public void DeleteCustomer(Customer customer)
        {
            customer.Deleted = true;
            Update(customer);
        }
    }
}
