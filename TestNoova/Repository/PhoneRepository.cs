﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Contracts;
using TestNoova.Models;

namespace TestNoova.Repository
{
    public class PhoneRepository : RepositoryBase<Phone>, IPhoneRepository
    {
        public PhoneRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<Phone> GetPhoneById(Guid phoneId)
        {
            return await FindByCondition(c => c.Id.Equals(phoneId))
                .FirstOrDefaultAsync();
        }

        public void CreatePhone(Phone phone)
        {
            Create(phone);
        }

        public void UpdatePhone(Phone phone)
        {
            Update(phone);
        }

        public void DeletePhone(Phone phone)
        {
            phone.Deleted = true;
            Update(phone);
        }

        public async Task<IEnumerable<Phone>> GetPhonesByCustomerId(Guid customerId)
        {
            return await FindByCondition(x => x.idCustomer.Equals(customerId))
                .ToListAsync();
        }
    }
}
