﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Contracts;
using TestNoova.Models;

namespace TestNoova.Repository
{
    public class ContactRepository : RepositoryBase<Contact>, IContactRepository
    {
        public ContactRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<Contact> GetContactById(Guid contactId)
        {
            return await FindByCondition(c => c.Id.Equals(contactId))
                .FirstOrDefaultAsync();
        }

        public void CreateContact(Contact contact)
        {
            Create(contact);
        }

        public void UpdateContact(Contact contact)
        {
            Update(contact);
        }

        public void DeleteContact(Contact contact)
        {
            contact.Deleted = true;
            Update(contact);
        }

        public async Task<IEnumerable<Contact>> GetContactsByCustomerId(Guid customerId)
        {
            return await FindByCondition(x => x.idCustomer.Equals(customerId))
                .ToListAsync();
        }
    }
}
