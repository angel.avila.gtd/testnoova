﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Contracts;
using TestNoova.Models;

namespace TestNoova.Repository
{
    public class AddressRepository : RepositoryBase<Address>, IAddressRepository
    {
        public AddressRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<Address> GetAddressById(Guid addressId)
        {
            return await FindByCondition(c => c.Id.Equals(addressId))
                .FirstOrDefaultAsync();
        }

        public void CreateAddress(Address address)
        {
            Create(address);
        }

        public void UpdateAddress(Address address)
        {
            Update(address);
        }

        public void DeleteAddress(Address address)
        {
            address.Deleted = true;
            Update(address);
        }

        public async Task<IEnumerable<Address>> GetAddressesByCustomerId(Guid customerId)
        {
            return await FindByCondition(x => x.idCustomer.Equals(customerId))
                .ToListAsync();
        }
    }
}
