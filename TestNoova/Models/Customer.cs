﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.Models
{
    [Table("Customer")]
    public class Customer
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public DateTime CreationDate { get; set; }

        public ICollection<Address> Addresses { get; set; }

        public ICollection<Phone> Phones { get; set; }

        public ICollection<Contact> Contacts { get; set; }

        public bool Deleted { get; set; }
    }
}
