﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.Models
{
    [Table("Address")]
    public class Address
    {
        public Guid Id { get; set; }

        public string Country { get; set; }

        public string Subdivision { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string ShortAddress { get; set; }

        [ForeignKey(nameof(Customer))]
        public Guid idCustomer { get; set; }
        public Customer Customer { get; set; }

        public bool Deleted { get; set; }
    }
}
