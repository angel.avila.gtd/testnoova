﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.Models
{
    [Table("Phone")]
    public class Phone
    {
        public Guid Id { get; set; }

        public string CountryCode { get; set; }

        public string SubdivisionCode { get; set; }

        public string PhoneNumber { get; set; }

        public string Extension { get; set; }

        [ForeignKey(nameof(Customer))]
        public Guid idCustomer { get; set; }
        public Customer Customer { get; set; }

        public bool Deleted { get; set; }
    }
}
