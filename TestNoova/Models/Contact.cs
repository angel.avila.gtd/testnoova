﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.Models
{
    [Table("Contact")]
    public class Contact
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Extension { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        [ForeignKey(nameof(Customer))]
        public Guid idCustomer { get; set; }
        public Customer Customer { get; set; }

        public bool Deleted { get; set; }
    }
}
