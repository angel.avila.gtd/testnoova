﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Models;

namespace TestNoova.Contracts
{
    public interface IPhoneRepository : IRepositoryBase<Phone>
    {
        Task<Phone> GetPhoneById(Guid phoneId);
        void CreatePhone(Phone phone);
        void UpdatePhone(Phone phone);
        void DeletePhone(Phone phone);
        Task<IEnumerable<Phone>> GetPhonesByCustomerId(Guid customerId);
    }
}
