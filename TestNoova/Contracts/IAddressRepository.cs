﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Models;

namespace TestNoova.Contracts
{
    public interface IAddressRepository : IRepositoryBase<Address>
    {
        Task<Address> GetAddressById(Guid addressId);
        void CreateAddress(Address address);
        void UpdateAddress(Address address);
        void DeleteAddress(Address address);
        Task<IEnumerable<Address>> GetAddressesByCustomerId(Guid customerId);
    }
}
