﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Models;

namespace TestNoova.Contracts
{
    public interface IContactRepository : IRepositoryBase<Contact>
    {
        Task<Contact> GetContactById(Guid contactId);
        void CreateContact(Contact contact);
        void UpdateContact(Contact contact);
        void DeleteContact(Contact contact);
        Task<IEnumerable<Contact>> GetContactsByCustomerId(Guid customerId);
    }
}
