﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestNoova.Contracts
{
    public interface IRepositoryWrapper
    {
        ICustomerRepository Customer { get; }
        IAddressRepository Address { get; }
        IContactRepository Contact { get; }
        IPhoneRepository Phone { get; }
        Task SaveAsync();
    }
}
