﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Models;

namespace TestNoova.Contracts
{
    public interface ICustomerRepository : IRepositoryBase<Customer>
    {
        Task<IEnumerable<Customer>> GetAllCustomers();
        Task<IEnumerable<Customer>> GetAllCustomersByFilter(string filter);
        Task<Customer> GetCustomerById(Guid customerId);
        void CreateCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
        void DeleteCustomer(Customer customer);
    }
}
