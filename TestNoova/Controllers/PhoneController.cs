﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Contracts;
using TestNoova.DataTransferObjects;
using TestNoova.Models;

namespace TestNoova.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PhoneController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private IMapper _mapper;

        public PhoneController(IRepositoryWrapper repoWrapper, IMapper mapper)
        {
            _repoWrapper = repoWrapper;
            _mapper = mapper;
        }

        [HttpGet("{phoneId}", Name = "PhoneById")]
        public async Task<IActionResult> GetPhoneById(Guid phoneId)
        {
            try
            {
                var phone = await _repoWrapper.Phone.GetPhoneById(phoneId);

                if (phone == null)
                {
                    return NotFound();
                }
                else
                {
                    var phoneResult = _mapper.Map<PhoneDto>(phone);
                    return Ok(phoneResult);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Phone GetPhoneById error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreatePhone([FromBody]NewPhoneDto phone)
        {
            try
            {
                if (phone == null)
                {
                    return BadRequest("El teléfono es nulo.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de teléfono invalido.");
                }

                var phoneEntity = _mapper.Map<Phone>(phone);

                _repoWrapper.Phone.CreatePhone(phoneEntity);
                await _repoWrapper.SaveAsync();

                var createdPhone = _mapper.Map<PhoneDto>(phoneEntity);

                return CreatedAtRoute("PhoneById", new { phoneId = createdPhone.Id }, createdPhone);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Phone CreatePhone error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{phoneId}")]
        public async Task<IActionResult> UpdatePhone(Guid phoneId, [FromBody]UpdatePhoneDto phone)
        {
            try
            {
                if (phone == null)
                {
                    return BadRequest("El teléfono es nulo.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de teléfono invalido.");
                }

                var phoneEntity = await _repoWrapper.Phone.GetPhoneById(phoneId);
                if (phoneEntity == null)
                {
                    return NotFound();
                }

                _mapper.Map(phone, phoneEntity);

                _repoWrapper.Phone.UpdatePhone(phoneEntity);
                await _repoWrapper.SaveAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Phone UpdatePhone error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{phoneId}")]
        public async Task<IActionResult> DeletePhone(Guid phoneId)
        {
            var phoneEntity = await _repoWrapper.Phone.GetPhoneById(phoneId);
            if (phoneEntity == null)
            {
                return NotFound();
            }
            _repoWrapper.Phone.DeletePhone(phoneEntity);
            await _repoWrapper.SaveAsync();

            return Ok(phoneEntity.Id);
        }
    }
}
