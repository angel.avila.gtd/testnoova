﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNoova.Contracts;
using TestNoova.DataTransferObjects;
using TestNoova.Models;

namespace TestNoova.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AddressController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private IMapper _mapper;

        public AddressController(IRepositoryWrapper repoWrapper, IMapper mapper)
        {
            _repoWrapper = repoWrapper;
            _mapper = mapper;
        }

        [HttpGet("{addressId}", Name = "AddressById")]
        public async Task<IActionResult> GetAddressById(Guid addressId)
        {
            try
            {
                var address = await _repoWrapper.Address.GetAddressById(addressId);

                if (address == null)
                {
                    return NotFound();
                }
                else
                {
                    var addressResult = _mapper.Map<AddressDto>(address);
                    return Ok(addressResult);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Address GetAddressById error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAddress([FromBody]NewAddressDto address)
        {
            try
            {
                if (address == null)
                {
                    return BadRequest("La dirección es nula.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de dirección invalido.");
                }

                var addressEntity = _mapper.Map<Address>(address);

                _repoWrapper.Address.CreateAddress(addressEntity);
                await _repoWrapper.SaveAsync();

                var createdAddress = _mapper.Map<AddressDto>(addressEntity);

                return CreatedAtRoute("AddressById", new { addressId = createdAddress.Id }, createdAddress);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Address CreateAddress error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{addressId}")]
        public async Task<IActionResult> UpdateAddress(Guid addressId, [FromBody]UpdateAddressDto address)
        {
            try
            {
                if (address == null)
                {
                    return BadRequest("La dirección es nula.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de dirección invalido.");
                }

                var addressEntity = await _repoWrapper.Address.GetAddressById(addressId);
                if (addressEntity == null)
                {
                    return NotFound();
                }

                _mapper.Map(address, addressEntity);

                _repoWrapper.Address.UpdateAddress(addressEntity);
                await _repoWrapper.SaveAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Address UpdateAddress error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{addressId}")]
        public async Task<IActionResult> DeleteAddress(Guid addressId)
        {
            var addressEntity = await _repoWrapper.Address.GetAddressById(addressId);
            if (addressEntity == null)
            {
                return NotFound();
            }
            _repoWrapper.Address.DeleteAddress(addressEntity);
            await _repoWrapper.SaveAsync();

            return Ok(addressEntity.Id);
        }
    }
}
