﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TestNoova.Contracts;
using TestNoova.DataTransferObjects;
using TestNoova.Models;

namespace TestNoova.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private IMapper _mapper;

        public CustomerController(IRepositoryWrapper repoWrapper, IMapper mapper)
        {
            _repoWrapper = repoWrapper;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCustomers()
        {
            try
            {
                var customersBase = await _repoWrapper.Customer.GetAllCustomers();
                var customers = _mapper.Map<IEnumerable<CustomerDto>>(customersBase);
                return Ok(customers);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer GetAllCustomers error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("filter/{filter}")]
        public async Task<IActionResult> GetAllCustomersByFilter(string filter)
        {
            try
            {
                var customersBase = await _repoWrapper.Customer.GetAllCustomersByFilter(filter);
                var customers = _mapper.Map<IEnumerable<CustomerDto>>(customersBase);
                return Ok(customers);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer GetAllCustomers error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{customerId}", Name = "CustomerById")]
        public async Task<IActionResult> GetCustomerById(Guid customerId)
        {
            try
            {
                var customer = await _repoWrapper.Customer.GetCustomerById(customerId);

                if (customer == null)
                {
                    return NotFound();
                }
                else
                {
                    var customerResult = _mapper.Map<CustomerDto>(customer);
                    return Ok(customerResult);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer GetCustomerById error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCustomer([FromBody]NewCustomerDto customer)
        {
            try
            {
                if (customer == null)
                {
                    return BadRequest("El cliente es nulo.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de cliente invalido.");
                }

                var customerEntity = _mapper.Map<Customer>(customer);

                _repoWrapper.Customer.CreateCustomer(customerEntity);
                await _repoWrapper.SaveAsync();

                var createdCustomer = _mapper.Map<CustomerDto>(customerEntity);

                if (customer.Address != null)
                {
                    customer.Address.IdCustomer = createdCustomer.Id;
                    var addressEntity = _mapper.Map<Address>(customer.Address);
                    _repoWrapper.Address.CreateAddress(addressEntity);
                    await _repoWrapper.SaveAsync();
                }

                if (customer.Contact != null)
                {
                    customer.Contact.IdCustomer = createdCustomer.Id;
                    var contactEntity = _mapper.Map<Contact>(customer.Contact);
                    _repoWrapper.Contact.CreateContact(contactEntity);
                    await _repoWrapper.SaveAsync();
                }

                if (customer.Phone != null)
                {
                    customer.Phone.IdCustomer = createdCustomer.Id;
                    var phoneEntity = _mapper.Map<Phone>(customer.Phone);
                    _repoWrapper.Phone.CreatePhone(phoneEntity);
                    await _repoWrapper.SaveAsync();
                }

                return CreatedAtRoute("CustomerById", new { customerId = createdCustomer.Id }, createdCustomer);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer CreateCustomer error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{customerId}")]
        public async Task<IActionResult> UpdateCustomer(Guid customerId, [FromBody]UpdateCustomerDto customer)
        {
            try
            {
                if (customer == null)
                {
                    return BadRequest("El cliente es nulo.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de cliente invalido.");
                }

                var customerEntity = await _repoWrapper.Customer.GetCustomerById(customerId);
                if (customerEntity == null)
                {
                    return NotFound();
                }

                _mapper.Map(customer, customerEntity);

                _repoWrapper.Customer.UpdateCustomer(customerEntity);
                await _repoWrapper.SaveAsync();

                var createdCustomer = _mapper.Map<CustomerDto>(customerEntity);

                return CreatedAtRoute("CustomerById", new { customerId }, createdCustomer);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer UpdateCustomer error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{customerId}")]
        public async Task<IActionResult> DeleteCustomer(Guid customerId)
        {
            var customerEntity = await _repoWrapper.Customer.GetCustomerById(customerId);
            if (customerEntity == null)
            {
                return NotFound();
            }
            _repoWrapper.Customer.DeleteCustomer(customerEntity);
            await _repoWrapper.SaveAsync();

            return Ok(customerEntity.Id);
        }

        [HttpGet("{customerId}/addresses")]
        public async Task<IActionResult> GetCustomerAddresses(Guid customerId)
        {
            try
            {
                var addressesBase = await _repoWrapper.Address.GetAddressesByCustomerId(customerId);
                var addresses = _mapper.Map<IEnumerable<AddressDto>>(addressesBase);
                return Ok(addresses);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer GetCustomerAddresses error: {ex}");
                return StatusCode(500, "Internal server error");
            }

        }

        [HttpGet("{customerId}/contacts")]
        public async Task<IActionResult> GetCustomerContacts(Guid customerId)
        {
            try
            {
                var contactsBase = await _repoWrapper.Contact.GetContactsByCustomerId(customerId);
                var contacts = _mapper.Map<IEnumerable<ContactDto>>(contactsBase);
                return Ok(contacts);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer GetCustomerContacts error: {ex}");
                return StatusCode(500, "Internal server error");
            }

        }

        [HttpGet("{customerId}/phones")]
        public async Task<IActionResult> GetCustomerPhones(Guid customerId)
        {
            try
            {
                var phonesBase = await _repoWrapper.Phone.GetPhonesByCustomerId(customerId);
                var phones = _mapper.Map<IEnumerable<PhoneDto>>(phonesBase);
                return Ok(phones);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Customer GetCustomerPhones error: {ex}");
                return StatusCode(500, "Internal server error");
            }

        }
    }
}