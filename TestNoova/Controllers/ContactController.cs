﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TestNoova.Contracts;
using TestNoova.DataTransferObjects;
using TestNoova.Models;

namespace TestNoova.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContactController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private IMapper _mapper;

        public ContactController(IRepositoryWrapper repoWrapper, IMapper mapper)
        {
            _repoWrapper = repoWrapper;
            _mapper = mapper;
        }

        [HttpGet("{contactId}", Name = "ContactById")]
        public async Task<IActionResult> GetContactById(Guid contactId)
        {
            try
            {
                var contact = await _repoWrapper.Contact.GetContactById(contactId);

                if (contact == null)
                {
                    return NotFound();
                }
                else
                {
                    var contactResult = _mapper.Map<ContactDto>(contact);
                    return Ok(contactResult);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Contact GetContactById error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateContact([FromBody]NewContactDto contact)
        {
            try
            {
                if (contact == null)
                {
                    return BadRequest("El contacto es nulo.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de contacto invalido.");
                }

                var contactEntity = _mapper.Map<Contact>(contact);

                _repoWrapper.Contact.CreateContact(contactEntity);
                await _repoWrapper.SaveAsync();

                var createdContact = _mapper.Map<ContactDto>(contactEntity);

                return CreatedAtRoute("ContactById", new { contactId = createdContact.Id }, createdContact);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Contact CreateContact error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{contactId}")]
        public async Task<IActionResult> UpdateContact(Guid contactId, [FromBody]UpdateContactDto contact)
        {
            try
            {
                if (contact == null)
                {
                    return BadRequest("El contacto es nulo.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Modelo de contacto invalido.");
                }

                var contactEntity = await _repoWrapper.Contact.GetContactById(contactId);
                if (contactEntity == null)
                {
                    return NotFound();
                }

                _mapper.Map(contact, contactEntity);

                _repoWrapper.Contact.UpdateContact(contactEntity);
                await _repoWrapper.SaveAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Contact UpdateContact error: {ex}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{contactId}")]
        public async Task<IActionResult> DeleteContact(Guid contactId)
        {
            var contactEntity = await _repoWrapper.Contact.GetContactById(contactId);
            if (contactEntity == null)
            {
                return NotFound();
            }
            _repoWrapper.Contact.DeleteContact(contactEntity);
            await _repoWrapper.SaveAsync();

            return Ok(contactEntity.Id);
        }
    }
}